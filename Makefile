.PHONY: all dockbuild build dev fig

# Docker and building not yet working
# all: dockbuild
all: dev

dockbuild:
	docker compose -f docker/docker-compose.yml up --build 

build:
	$(MAKE) fig MANIM_QUALITY=-qh
	@quarto render main.qmd --to revealjs

# $(MAKE) fig MANIM_QUALITY=-ql
dev:
	@quarto preview main.qmd --to revealjs

fig:
	@for figfile in code/manim/*; do \
		manim $$figfile \
		  $(MANIM_QUALITY) \
		  --transparent \
			--format gif \
			--progress_bar none \
			--log_to_file \
		  --write-all \
			-o ../../$$name.gif; \
	done
