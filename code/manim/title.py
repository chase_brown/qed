from manim import *

class Title(Scene):
    def construct(self):
        title = Text("Main title text", font_size=96)
        author = Text("Author text", font_size=62).next_to(title, DOWN)
        self.play(Write(title), Write(author))
        self.wait()
