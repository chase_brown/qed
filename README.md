![qedlogo](asset/static/img/qed_logo.png)

# `qed`: Elite presentations

> — Quod erat demonstrandum —

This project is for the elite. The few, the proud, the `vim`.

`qed` creates presentations with a deeply engrained foss/unix-mentality tech-stack:

1. Quarto: Data-driven presentations from code (for git-compatibility)
2. Plotly: Interactive data visualizations for max information density
3. Manim: Exposition for the modern world with a focus on math

## Table of Contents

1. [Installation](#floppy_disk-installation)
2. [Usage](#computer-usage)
3. [Development](#hammer_and_wrench-development)
4. [Notes](#clipboard-notes)
5. [License](#scales-license)

## :floppy_disk: Installation

Typical installation requires `quarto`, `manim`, and `plotly`, though this
depends completely on how you use it.

## :computer: Usage

#### To create and run the presentation:

```bash
make && firefox main.html
```

## Goals for project

This project started when working on an improvement to my typical Google slide
presentations for my dissertation defense. Those slides are also intended to
be FOSS as well, and should be located
[here](https://gitlab.com/chase_brown/defense_presentation) in the near future.

### Short-term goal

The idea is of course very simple, so this project is not really needed, if you
know quarto well. However, the intent for this repo short-term is to keep this as
a _template_ such that useful snippets for how to use quarto and make effective
presentations quickly is always available. So this is something like a
"cheat-sheet" for quarto.

## Long-term goal

However, the goal of this project is to also be able to make _templates_ (not
whole projects) for certain presentations, via interaction with an LM.
Therefore, the `scratchpad` is provided if this repo is run within a
`qemu-nested-docker` (docker within a vm) a local LM can use the scratchpad and
write out good templates for the presentation and then provide them when they
are decent. (Don't expect much from this process, however)

This keeps the presentation construction completely confined to text only for generating
output, which is far more resource friendly, and has better chances of providing what is desired.

---

### :exclamation: Troubleshooting

Some issues with the font for this example may come up.

The font can be downloaded from [here](
https://fonts.google.com/specimen/Rosarivo).

## :scales: License

See the `LICENSE` file for details
